/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1;

import utilitario.Utilitario;
import ex1.LuckyNumber;
import ex2.PerfectNum;
import ex3.GuessNumber;
import ex4.Words;
import ex5.Control;
import java.util.Scanner;

/**
 *
 * @author Kath
 */
public class Tarea1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("1. Averigüe su número de la suerte!");
        int day = Utilitario.capturaValorEntero("Ingrese su día de nacimiento:",1,31);
        int month = Utilitario.capturaValorEntero("Ingrese su mes de nacimiento:",1,12);
        int year = Utilitario.capturaValorEntero("Ingrese su año de nacimiento:",1,2020);
        LuckyNumber luckyNum = new LuckyNumber(day, month, year);
        System.out.println("Su número de la suerte es: "+luckyNum.calcLuckyNum());
       
        System.out.println("\n2. Averigüe si un número es perfecto.");
        PerfectNum perfNum = new PerfectNum();
        int num = Utilitario.capturaValorEntero("Ingrese un número:", 1, 10000);
        System.out.println(perfNum.perfectNumber(num));
        
        System.out.println("\n3. Adivine el número.");
        GuessNumber guess = new GuessNumber();
        System.out.println(guess.guess());

        System.out.println("\n4. Compruebe si las palabras riman o no.");
        String word1 = Utilitario.capturaValorString("Ingrese la primera palabra:");
        String word2 = Utilitario.capturaValorString("Ingrese la segunda palabra:");
        Words words = new Words(word1,word2);
        System.out.println(words.rhyme());
        
        System.out.println("\n5. Sistema de control de choferes");
        Control control = new Control();
        control.readArhive();
        int idNumber = Utilitario.capturaEntrada("Ingrese el número de cédula del trabajador: ");
        String strId = Integer.toString(idNumber);
        System.out.println(control.searchId(strId));
    }

}
