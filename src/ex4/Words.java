/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex4;

/**
 *
 * @author Kath
 */
public class Words {

    private String word1;
    private String word2;

    public Words(String word1, String word2) {
        this.word1 = word1;
        this.word2 = word2;
    }

    public String rhyme() {
        String result = "";
        String last3Letters1 = "";
        int sizeW1 = this.word1.length();
        last3Letters1 = this.word1.substring(sizeW1 - 3, sizeW1);
        String last2Letters1 = this.word1.substring(sizeW1 - 2, sizeW1);

        String last3Letters2 = "";
        int sizeW2 = this.word2.length();
        last3Letters2 = this.word2.substring(sizeW2 - 3, sizeW2);
        String last2Letters2 = this.word2.substring(sizeW2 - 2, sizeW2);

        if (last3Letters1.equals(last3Letters2)) {
            result = "Las palabras " + this.word1 + " y " + this.word2 + " riman completamente.";
        } else if (last2Letters1.equals(last2Letters2)) {
            result = "Las palabras " + this.word1 + " y " + this.word2 + " medio riman.";

        } else {
            result = "Las palabras " + this.word1 + " y " + this.word2 + " no riman.";

        }
        return result;

    }
}
