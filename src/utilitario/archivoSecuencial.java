package utilitario;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class archivoSecuencial {

    private String directorio;
    private String nombre;
    private String errorMsg;
    private boolean error;

    public archivoSecuencial(String pPath, String pNombre) {
        this.directorio = pPath;
        this.nombre = pNombre;
        this.error = false;
        this.errorMsg = "";
    }

    public void setDirectorio(String path) {
        this.directorio = path;
    }

    public String getDirectorio() {
        return directorio;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public boolean crear() {
        boolean creado = false;
        try {
            File archivo = new File(this.getDirectorio(), this.getNombre());
            creado = archivo.createNewFile();
        } catch (Exception e) {
            this.error = true;
            this.errorMsg
                    = "Ha ocurrido un error al tratar de crear el archivo, detalle t�cnico del error: "
                    + e.getMessage();
        }
        return creado;
    }

    public boolean borrar() {
        boolean borrado = false;
        try {
            File archivo = new File(this.getDirectorio(), this.getNombre());
            borrado = archivo.delete();
        } catch (Exception e) {
            this.error = true;
            this.errorMsg
                    = "Ha ocurrido un error al tratar de borrar el archivo, detalle t�cnico del error: "
                    + e.getMessage();
        }
        return borrado;
    }

    public String leer() {
        String contenido = "";
        try {
            File archivoEntrada = new File(this.getDirectorio(), this.getNombre());
            FileReader archivoLector = new FileReader(archivoEntrada);
            BufferedReader archivo = new BufferedReader(archivoLector);
            String linea;
            while ((linea = archivo.readLine()) != null) {
                contenido += linea + "\n";
            }
            archivo.close();
        } catch (IOException e) {
            this.error = true;
            this.errorMsg
                    = "Ha ocurrido un error al tratar de leer el archivo, detalle t�cnico del error: "
                    + e.getMessage();
        }
        return contenido;
    }
    
    public String[] leerArray(){
        return this.leer().split("\n");
    }
    
    public int obtenerCantidadLineas(){
        return this.leerArray().length;
    }

    public void escribir(String pTexto) {
        try {
            File archivoSalida = new File(this.getDirectorio(), this.getNombre());
            BufferedWriter outfile = new BufferedWriter(new FileWriter(archivoSalida));
            outfile.write(pTexto);
            outfile.close();
        } catch (IOException e) {
            this.error = true;
            this.errorMsg
                    = "Ha ocurrido un error al tratar de leer el archivo, detalle t�cnico del error: "
                    + e.getMessage();
        }
    }

    public void agregarLineaAlFinal(String pTexto) {
        try {
            String contenidoActual = this.leer();
            contenidoActual += "\n" + pTexto;
            this.escribir(contenidoActual);
        } catch (Exception e) {

        }
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public boolean isError() {
        return error;
    }
}
