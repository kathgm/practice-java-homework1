/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.util.Scanner;

/**
 *
 * @author Kath
 */
public class Utilitario {

    public static int capturaEntrada(String pTextoPeticion) {
        int valorRetorno = 0;
        boolean continuar = true;
        String entrada = "";
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true) {
            try {
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                entrada = otroTeclado.next();
                if (!entrada.toUpperCase().equals("X")) {
                    valorRetorno = Integer.parseInt(entrada);
                    if (valorRetorno >= 1) {
                        continuar = false;
                    } else {
                        System.out.println("El valor debe ser mayor a 1");
                    }
                } else {
                    System.out.println("Salió.");
                    continuar = false;
                    entrada = "X";
                }

            } catch (Exception e) {
                System.out.println("Valor ingresado incorrecto, debe digitar un valor entero o una X para salir!!");
            }
        }
        return valorRetorno;
    }

    public static int capturaValorEntero(String pTextoPeticion, int pValorMinimo, int pValorMaximo) {
        int valorRetorno = 0;
        boolean continuar = true;
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true) {
            try {
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                valorRetorno = otroTeclado.nextInt();

                if (valorRetorno >= pValorMinimo) {
                    continuar = false;
                } else {
                    System.out.print("El valor debe ser mayor a: " + pValorMinimo);
                }
                continuar = false;
            } catch (Exception e) {
                System.out.println("Valor ingresado incorrecto, debe digitar un valor entero!!");
            }
        }
        return valorRetorno;
    }

    public static double capturaValorDouble(String pTextoPeticion) {
        double valorRetorno = 0;
        boolean continuar = true;
        Scanner otroTeclado = new Scanner(System.in);
        while (continuar == true) {
            try {
                System.out.println(pTextoPeticion);
                otroTeclado = new Scanner(System.in);
                valorRetorno = otroTeclado.nextDouble();
                continuar = false;
            } catch (Exception e) {
                System.out.println("Valor ingresado incorrecto, debe digitar un valor dividido por , ");
            }
        }
        return valorRetorno;
    }

    public static String capturaValorString(String pTextoPeticion) {
        String retorno = "";
        Scanner otroTeclado = new Scanner(System.in);
        System.out.println(pTextoPeticion);
        retorno = otroTeclado.nextLine();
        return retorno;
    }

    public static char capturaValorChar(String pTextoPeticion) {
        char caracter;
        System.out.println(pTextoPeticion);
        Scanner otroTeclado = new Scanner(System.in);
        caracter = otroTeclado.next().charAt(0);
        return caracter;
    }

}
