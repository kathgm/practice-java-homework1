/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex5;

/**
 *
 * @author Kath
 */
public class Archivo {
    private String name;
    private String id;
    private String vehicle;
    private String cargo;

    public Archivo(String name, String id, String vehicle, String cargo) {
        this.name = name;
        this.id = id;
        this.vehicle = vehicle;
        this.cargo = cargo;
    }
    
    @Override
    public String toString(){
        String strArchive="";
        strArchive += "\nNombre: "+this.name+"\nCédula: "+this.id+"\nTipo de Camión: "+this.vehicle+"\nTipo de Carga: "+this.cargo;
        return strArchive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    
    
}
