/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author Kath
 */
public class LuckyNumber {

    private int day;
    private int month;
    private int year;

    public LuckyNumber(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int calcLuckyNum() {
        int sumDate = this.day + this.month + this.year;
        String date = Integer.toString(sumDate);
        char num1 = date.charAt(0);
        char num2 = date.charAt(1);
        char num3 = date.charAt(2);
        char num4 = date.charAt(3);
        int value1 = Character.getNumericValue(num1);
        int value2 = Character.getNumericValue(num2);
        int value3 = Character.getNumericValue(num3);
        int value4 = Character.getNumericValue(num4);
        int luckyNumber = value1 + value2 + value3 + value4;
        return luckyNumber;
    }

    public int getDia() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
