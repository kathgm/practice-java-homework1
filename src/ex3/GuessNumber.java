/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex3;
import utilitario.Utilitario;
import java.util.Random;

/**
 *
 * @author Kath
 */
public class GuessNumber {

    
    private int numeroAdivinar;

    public GuessNumber() {
        Random oRandom = new Random();
        this.numeroAdivinar = oRandom.nextInt(20);
    }
    
    public String guess(){
        String result = "";
        int cont = 0;
        for (int i = 0; i < 4; i++) {
            cont++;
            int entradaUsuario = Utilitario.capturaValorEntero("Ingrese un número, intento "+cont+" de 4.",1,20);
            
            if (entradaUsuario==this.numeroAdivinar ) {
                result = "Felicidades!! Adivinó el número.";
                break;
            }
            result="No ha adividinado. El número era el: "+this.numeroAdivinar;
        }
        return result;
    }

    
}
