/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex2;

/**
 *
 * @author Kath
 */
public class PerfectNum {

    private int num;
    private int divider;
    private int collector;

    public PerfectNum() {
        this.divider = 1;
        this.collector = 0;
    }

    public String perfectNumber(int num) {
        String result = "";
        while (this.divider < num) {
            if (num % this.divider == 0) {
                this.collector += this.divider;
            }
            this.divider += 1;
        }
        if (this.collector == num) {
            result = "El número es perfecto.";
        } else {
            result = "El número NO es perfecto.";

        }
        return result;
    }

}
